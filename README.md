## dwm 6.2

#### Patches:
* fullscreen
* vanitygaps
* dylanaraps
* winicon
* cool autostart
* active tag indicator bar

### Dependency:
```
# Arch
sudo pacman -S imlib2
  
# Debian
sudo apt install libimlib2-dev
```

![Screenshot](/desktop.png)

based on: https://github.com/igor37/dwm-rounded-corners
